def convert_to_roman (integer):
    dict_hundreds = {0: '', 1: 'C', 2: 'CC', 3: 'CCC', 4:'CD', 5: 'D', 6: 'DC', 7: 'DCC', 8:'DCCC', 9: 'CM'}
    dict_tenths = {0: '', 1: 'X', 2: 'XX', 3: 'XXX', 4:'XL', 5: 'L', 6: 'LX', 7: 'LXX', 8:'LXXX', 9: 'XC'}
    dict_ones = {0: '', 1: 'I', 2: 'II', 3: 'III', 4:'IV', 5: 'V', 6: 'VI', 7: 'VII', 8:'VIII', 9: 'IX'}

    roman = '' # create empty variable for the roman number
    integer = round(integer) # create an integer without a decimal point, transform it to a string
    string_integer = str(integer) #  transform integer to type string, otherwise its length cannot be calculated
    length_int = len(string_integer)

    if length_int >= 4: 
        thousands = string_integer[:-3]
        roman += int(thousands) * 'M'
    if length_int >= 3:
        hundreds = string_integer[-3]
        roman += dict_hundreds[int(hundreds)]
    if length_int >= 2:
        tenths = string_integer[-2]
        roman += dict_tenths[int(tenths)]
    ones = string_integer[-1]
    roman += dict_ones[int(ones)]

    return roman

# execute function
print(convert_to_roman(235.56))